//
//  ProductMenu.h
//  arneg
//
//  Created by Fabio on 03/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductEntity.h"

@interface ProductMenu : UIView

@property NSMutableArray *buttons;
@property int intActive;
@property ProductEntity *product;
@property UINavigationController *nav;

- (void) setMenuActive:(int)n;
- (void) align;

@property UIButton *personalizarBtn;
@property UIButton *opcionaisBtn;
@property UIButton *especificacoesBtn;
@property UIButton *fotosBtn;

- (IBAction)goPersonalizar:(id)sender;
- (IBAction)goOpcionais:(id)sender;
- (IBAction)goEspecificacoes:(id)sender;
- (IBAction)goFotos:(id)sender;

@end
