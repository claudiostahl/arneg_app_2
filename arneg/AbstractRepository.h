//
//  AbstractRepository.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AbstractRepository : NSObject

@property NSMutableArray *data;
@property NSString *resource;

- (NSArray *)loadDataResource;

@end
