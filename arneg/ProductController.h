//
//  ProductController.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractController.h"

@interface ProductController : AbstractController

@property (weak, nonatomic) IBOutlet UIView *galleryView;
@property (weak, nonatomic) IBOutlet UIView *galleryNavigation;

@property int galleryIndex;
@property NSMutableArray *galleryItems;
@property NSMutableArray *galleryNavigationItems;

- (void)openSubproduct:(id)sender;

@end
