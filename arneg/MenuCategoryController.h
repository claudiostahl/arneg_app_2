//
//  MenuCategoryController.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCategoryController : UIViewController

- (void)openProduct:(id)sender;

@end
