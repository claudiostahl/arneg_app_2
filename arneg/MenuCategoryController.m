//
//  MenuCategoryController.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "MenuCategoryController.h"
#import "CategoryEntity.h"
#import "CategoryRepository.h"
#import "ProductController.h"
#import "AbstractController.h"
#import "ProductRepository.h"

@implementation MenuCategoryController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"popover 1");
    
    CategoryRepository *repo = [[CategoryRepository alloc] init];
    NSArray *data = [[NSArray alloc] initWithArray:repo.data];
    
    int i = 0;
    for (CategoryEntity *entity in data) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btn.frame = CGRectMake(30, i*50+15, 200, 50);
        [btn setTitle:entity.title forState:UIControlStateNormal];
        UIColor *color = [UIColor colorWithRed:65/255.0f green:64/255.0f blue:66/255.0f alpha:1.0f];
        [btn setTitleColor:color forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:18];
        //btn.backgroundColor = [UIColor blueColor];
        btn.tag = entity.categoryId;
        [btn addTarget:self action:@selector(openProduct:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        i++;
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    //NSLog(@"popover 2");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openProduct:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    int catId = (int) btn.tag;
    
    ProductRepository *repo = [[ProductRepository alloc] init];
    if ([[repo findByCategoryId:catId] count] == 0)
        return;
    ProductEntity *currentProduct = [repo findByCategoryId:catId][0];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AbstractController *controller;
    if (!([currentProduct.imageCustom1 isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductController"];
    } else if (!([currentProduct.imageOptions isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductOptionController"];
    } else if (!([currentProduct.imageSpecification isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductSpecificationController"];
    } else if (currentProduct.photos.count > 0) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductGalleryController"];
    } else {
        return;
    }
    
    controller.product = currentProduct;
    
    [AbstractController.instance.navigationController pushViewController:controller animated:YES];
    [AbstractController.instance.menuCategoryPopover dismissPopoverAnimated:YES];
}

@end
