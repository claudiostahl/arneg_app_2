//
//  HomeController.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractController.h"
#import "CategoryEntity.h"
#import "CategoryRepository.h"
#import "arneg-Swift.h"

@interface MainController : AbstractController

@property AppBridge *bridge;

@property (weak, nonatomic) IBOutlet UILabel *labelLoad;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
- (IBAction)openArpejo:(id)sender;

- (void)recognizerSingleTap:(UITapGestureRecognizer *)recognizer;
- (void) setLabelInfo:(NSString *)info;
- (void) threadSetLabelInfo:(NSString *)info;
- (void) enableClick;

@end
