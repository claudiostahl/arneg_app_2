//
//  Subproduct.h
//  arneg
//
//  Created by Fabio on 19/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subproduct : NSObject

@property int subproductId;
@property NSString *title;
@property NSString *description;
@property NSString *imageCustom1;
@property NSString *imageCustom2;
@property NSString *imageCustom3;

@end
