import Foundation
import UIKit
import RealmSwift

public struct AppService {
    
    static let server   = AppServer()
    static let user     = AppUser()
    static let util     = AppUtil()
    
    static func realm() -> Realm {
        let config = Realm.Configuration(schemaVersion: 3)
        let realm = try! Realm(configuration: config)
        return realm
    }
    
}