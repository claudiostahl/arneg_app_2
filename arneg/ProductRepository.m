//
//  ProductRepository.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductRepository.h"
#import "ProductEntity.h"
#import "Subproduct.h"
#import "ProductOptionEntity.h"
#import "ProductPhotoEntity.h"

@implementation ProductRepository

static NSMutableArray *cache;

- (id) init {
    self = [super init];
    
    if (!cache) {
        NSLog(@"product load");
        self.resource = @"products.plist";
        self.data = [[NSMutableArray alloc] init];
        
        NSArray *dataResource = [self loadDataResource];
        
        for (NSDictionary *item in dataResource) {
            ProductEntity *entity = [[ProductEntity alloc] init];
            [entity setProductId: (int)[[item objectForKey:@"id"] integerValue]];
            [entity setCategoryId: (int)[[item objectForKey:@"category_id"] integerValue]];
            [entity setTitle:[item objectForKey:@"title"]];
            [entity setDescription:[item objectForKey:@"description"]];
            [entity setThumb:[item objectForKey:@"thumb"]];
            [entity setImageCustom1:[item objectForKey:@"image_custom1"]];
            [entity setImageCustom2:[item objectForKey:@"image_custom2"]];
            [entity setImageCustom3:[item objectForKey:@"image_custom3"]];
            [entity setImageSpecification:[item objectForKey:@"image_specification"]];
            [entity setImageOptions:[item objectForKey:@"image_options"]];
            [entity setImageOptionsNormal:[item objectForKey:@"image_options_normal"]];
            
            NSArray *dataPhotos = (NSArray *) [item objectForKey:@"photos"];
            entity.photos = [[NSMutableArray alloc] init];
            for (NSDictionary *itemPhoto in dataPhotos) {
                ProductPhotoEntity *photo = [[ProductPhotoEntity alloc] init];
                [photo setPhotoId: (int)[[itemPhoto objectForKey:@"photo_id"] integerValue]];
                [photo setThumb:[itemPhoto objectForKey:@"thumb"]];
                [photo setImage:[itemPhoto objectForKey:@"file"]];
                [photo setDescription:[itemPhoto objectForKey:@"description"]];
                [entity.photos addObject:photo];
            }
            
            NSArray *dataOptions = (NSArray *) [item objectForKey:@"options"];
            entity.options = [[NSMutableArray alloc] init];
            for (NSDictionary *itemOtion in dataOptions) {
                ProductOptionEntity *option = [[ProductOptionEntity alloc] init];
                [option setOptionId: (int)[[itemOtion objectForKey:@"product_related_id"] integerValue]];
                [option setTitle:[itemOtion objectForKey:@"title"]];
                [option setDescription:[itemOtion objectForKey:@"description"]];
                [option setStandard: (bool)[[itemOtion objectForKey:@"standard"] integerValue]];
                [option setImage1: [itemOtion objectForKey:@"image1"]];
                [option setImage2: [itemOtion objectForKey:@"image2"]];
                [option setImage3: [itemOtion objectForKey:@"image3"]];
                [option setX: (int)[[itemOtion objectForKey:@"x"] integerValue]];
                [option setY: (int)[[itemOtion objectForKey:@"y"] integerValue]];
                [entity.options addObject:option];
            }
            
            NSArray *dataSubs = (NSArray *) [item objectForKey:@"subproducts"];
            entity.subproducts = [[NSMutableArray alloc] init];
            for (NSDictionary *itemSub in dataSubs) {
                Subproduct *sub = [[Subproduct alloc] init];
                [sub setSubproductId: (int)[[itemSub objectForKey:@"subproduct_id"] integerValue]];
                [sub setTitle:[itemSub objectForKey:@"title"]];
                [sub setImageCustom1:[itemSub objectForKey:@"image_custom1"]];
                [sub setImageCustom2:[itemSub objectForKey:@"image_custom2"]];
                [sub setImageCustom3:[itemSub objectForKey:@"image_custom3"]];
                [entity.subproducts addObject:sub];
            }

            [self.data addObject:entity];
        }
        
        if (self.data.count > 0)
            cache = self.data;
        
        //NSLog(@"%@", self.data);
        
    } else {
        NSLog(@"product cache");
        self.data = cache;
    }
    
    return self;
}

- (NSMutableArray *) findByCategoryId:(int) categoryId {
    NSMutableArray *out = [[NSMutableArray alloc] init];
    for (ProductEntity *p in self.data) {
        if (p.categoryId == categoryId) {
            [out addObject:p];
        }
    }
    return out;
}

- (ProductEntity *) loadById:(int) productId {
    ProductEntity *productOut;
    for (ProductEntity *p in self.data) {
        if (p.productId == productId) {
            productOut = p;
            break;
        }
    }
    return productOut;
}

@end
