//
//  ProductController.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductController.h"
#import "CategoryEntity.h"
#import "ProductEntity.h"
#import "Subproduct.h"
#import "CategoryRepository.h"
#import "ProductRepository.h"
#import "MenuProductController.h"

@interface ProductController()

- (void)nextItemGallery:(UITapGestureRecognizer *)recognizer;

@end

@implementation ProductController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (self.product == nil) {
        ProductRepository *repo = [[ProductRepository alloc] init];
        self.product = [repo findByCategoryId:self.categoryId][0];
    }
    
    //CONFIG SUBS
    if ([self.product.subproducts count]) {
        NSMutableArray *labels = [[NSMutableArray alloc] init];
        [labels addObject:self.product.description];
        for (Subproduct *sub in self.product.subproducts) {
            [labels addObject:sub.title];
        }
        UISegmentedControl *control = [[UISegmentedControl alloc] initWithItems:labels];
        control.tintColor = [UIColor whiteColor];
        CGRect f = control.frame;
        f.origin.x = 1024 - f.size.width - 40;
        f.origin.y = 607;
        [control setFrame:f];
        [self.view addSubview:control];
        [control addTarget:self action:@selector(openSubproduct:) forControlEvents:UIControlEventValueChanged];
        [control setSelectedSegmentIndex:0];
    }
    
    //CONFIG GALLERY
    self.galleryView.frame = CGRectMake(0, 0, 1024, 704);
    self.galleryView.backgroundColor = nil;
    self.galleryNavigation.backgroundColor = nil;
    self.galleryIndex = 0;
    self.galleryItems = [[NSMutableArray alloc] init];
    self.galleryNavigationItems = [[NSMutableArray alloc] init];
    NSArray *colors = [[NSArray alloc] initWithObjects:
                       [UIColor colorWithRed:62/255.0f green:62/255.0f blue:62/255.0f alpha:1.0f],
                       [UIColor colorWithRed:198/255.0f green:158/255.0f blue:106/255.0f alpha:1.0f],
                       [UIColor colorWithRed:178/255.0f green:178/255.0f blue:178/255.0f alpha:1.0f],
                       nil];
    
    NSMutableArray *imgs = [[NSMutableArray alloc] init];
    if (!([self.product.imageCustom1 isEqual: @""]))
        [imgs addObject:self.product.imageCustom1];
    if (!([self.product.imageCustom2 isEqual: @""]))
        [imgs addObject:self.product.imageCustom2];
    if (!([self.product.imageCustom3 isEqual: @""]))
        [imgs addObject:self.product.imageCustom3];
    
    int i = 0;
    for (NSString *img in imgs) {
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:img]];
        imgView.frame = CGRectMake(0, 0, 1024, 704);
        imgView.alpha = 0;
        [self.galleryItems addObject:imgView];
        [self.galleryView addSubview:imgView];
        
        UIView *ball = [[UIView alloc] init];
        ball.tag = i;
        ball.frame = CGRectMake(i*50, 0, 36, 36);
        ball.layer.cornerRadius = 18;
        ball.layer.borderColor = [UIColor whiteColor].CGColor;
        [ball setBackgroundColor: colors[i] ];
        UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextItemGallery:)];
        [ball addGestureRecognizer:singleFingerTap];
        [self.galleryNavigationItems addObject:ball];
        
        if (imgs.count > 1)
            [self.galleryNavigation addSubview:ball];
        
        i++;
    }
    
    UIImageView *slide1 = self.galleryItems[0];
    slide1.alpha = 1;
    UIView *ball = self.galleryNavigationItems[0];
    ball.layer.borderWidth = 2.0f;
    
    [self configProductMenus:0];
}

- (void)viewDidAppear:(BOOL)animated {
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)viewDidDisappear:(BOOL)animated {
    self.galleryItems = nil;
}

- (void)nextItemGallery:(UITapGestureRecognizer *)recognizer
{
    UIView *ballCurrent = self.galleryNavigationItems[self.galleryIndex];
    ballCurrent.layer.borderWidth = 0;
    UIView *ball = recognizer.view;
    ball.layer.borderWidth = 2.0f;
    
    UIImageView *slide1 = self.galleryItems[self.galleryIndex];
    self.galleryIndex = (int) ball.tag;
    UIImageView *slide2 = self.galleryItems[self.galleryIndex];
    
    [UIView animateWithDuration:0.5 animations:^ {
        slide1.alpha = 0;
        slide2.alpha = 1;
    }];
}

- (void)openSubproduct:(UISegmentedControl *)segment
{
    if (segment.selectedSegmentIndex == 0) {
        UIImageView *img0 = self.galleryItems[0];
        UIImageView *img1 = self.galleryItems[1];
        UIImageView *img2 = self.galleryItems[2];
        [img0 setImage:[UIImage imageNamed: self.product.imageCustom1] ];
        [img1 setImage:[UIImage imageNamed: self.product.imageCustom2] ];
        [img2 setImage:[UIImage imageNamed: self.product.imageCustom3] ];
        
    } else {
        int i = (int) (segment.selectedSegmentIndex - 1);
        Subproduct *sub = self.product.subproducts[i];
        UIImageView *img0 = self.galleryItems[0];
        UIImageView *img1 = self.galleryItems[1];
        UIImageView *img2 = self.galleryItems[2];
        [img0 setImage:[UIImage imageNamed: sub.imageCustom1] ];
        [img1 setImage:[UIImage imageNamed: sub.imageCustom2] ];
        [img2 setImage:[UIImage imageNamed: sub.imageCustom3] ];
    }
}

@end