//
//  ProductPhotoEntity.m
//  arneg
//
//  Created by Fabio on 05/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductPhotoEntity.h"

@implementation ProductPhotoEntity

@synthesize description, thumb, image, photoId, position;

@end
