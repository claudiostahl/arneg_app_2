//
//  AbstractRepository.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "AbstractRepository.h"

@implementation AbstractRepository

- (NSArray *) loadDataResource {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:self.resource];
    NSArray *array = [NSArray arrayWithContentsOfFile:path];
    
    ///NSLog(@"%@", array);
    
    return array;
}

@end