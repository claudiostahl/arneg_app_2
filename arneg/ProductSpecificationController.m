//
//  ProductSpecificationController.m
//  arneg
//
//  Created by Fabio on 03/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductSpecificationController.h"

@implementation ProductSpecificationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configProductMenus:2];
    
    UIImage *img = [UIImage imageNamed: self.product.imageSpecification];
    
    int w = img.size.width;
    int h = img.size.height;
    int nw = 944;
    int nh = nw * h / w;
    
    self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(40, 40, nw, nh)];
    [self.imgView setImage:img];
    [self.scrollView addSubview:self.imgView];
    
    self.scrollView.contentSize = CGSizeMake(1024, self.imgView.frame.size.height+120);
    
    //modal
    self.modalView.layer.masksToBounds = NO;
    self.modalView.layer.shadowOffset = CGSizeMake(0, 0);
    self.modalView.layer.shadowRadius = 10;
    self.modalView.layer.shadowOpacity = 0.8;
    self.modalView.hidden = true;
    self.modalBg.hidden = true;
    
    //product
    self.produtoBtn.transform = CGAffineTransformTranslate(self.produtoBtn.transform, -50, 0);
    self.produtoBtn.transform = CGAffineTransformRotate(self.produtoBtn.transform, M_PI / -2);
    self.produtoImg.hidden = true;
    
    //caracteristicas
    self.caracteristicaBtn.transform = CGAffineTransformTranslate(self.caracteristicaBtn.transform, -50, 145);
    self.caracteristicaBtn.transform = CGAffineTransformRotate(self.caracteristicaBtn.transform, M_PI / -2);
    self.caracteristicaImg.hidden = true;
    
    //refrigeracao
    self.refrigeracaoBtn.transform = CGAffineTransformTranslate(self.refrigeracaoBtn.transform, -50, 290);
    self.refrigeracaoBtn.transform = CGAffineTransformRotate(self.refrigeracaoBtn.transform, M_PI / -2);
    self.refrigeracaoImg.hidden = true;
    
    [self.modalBg addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recognizerSingleTap:)] ];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.imgView setImage:nil];
}

- (IBAction)openModalProduto:(id)sender {
    self.caracteristicaImg.hidden = true;
    self.refrigeracaoImg.hidden = true;
    if (self.produtoImg.hidden == true) {
        self.modalView.hidden = false;
        self.produtoImg.hidden = false;
        self.modalBg.hidden = false;
    } else {
        self.modalView.hidden = true;
        self.produtoImg.hidden = true;
        self.modalBg.hidden = true;
    }
}

- (IBAction)openModalCaracteristica:(id)sender {
    self.produtoImg.hidden = true;
    self.refrigeracaoImg.hidden = true;
    if (self.caracteristicaImg.hidden == true) {
        self.modalView.hidden = false;
        self.caracteristicaImg.hidden = false;
        self.modalBg.hidden = false;
    } else {
        self.modalView.hidden = true;
        self.caracteristicaImg.hidden = true;
        self.modalBg.hidden = true;
    }
}

- (IBAction)openModalRefrigeracao:(id)sender {
    self.caracteristicaImg.hidden = true;
    self.produtoImg.hidden = true;
    if (self.refrigeracaoImg.hidden == true) {
        self.modalView.hidden = false;
        self.refrigeracaoImg.hidden = false;
        self.modalBg.hidden = false;
    } else {
        self.modalView.hidden = true;
        self.refrigeracaoImg.hidden = true;
        self.modalBg.hidden = true;
    }
}

- (void)recognizerSingleTap:(UITapGestureRecognizer *)recognizer {
    self.modalView.hidden = true;
    self.modalBg.hidden = true;
}

@end
