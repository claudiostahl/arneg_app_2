//
//  Category.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "CategoryEntity.h"

@implementation CategoryEntity

@synthesize categoryId, title, description, image, position;

@end