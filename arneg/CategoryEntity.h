//
//  Category.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryEntity : NSObject

@property int categoryId;
@property NSString *title;
@property NSString *description;
@property NSString *image;
@property int position;

@end