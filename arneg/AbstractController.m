//
//  AbstractController.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//
#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

#import "AbstractController.h"
#import "MainController.h"
#import "CategoryController.h"
#import "ProductController.h"
#import "MenuCategoryController.h"
#import "MenuProductController.h"
#import "CategoryPopoverBackgroundView.h"
#import "ProductPopoverBackgroundView.h"
#import "ProductMenu.h"
#import "CategoryThumb.h"
#import "Reachability.h"
#import "ProductRepository.h"

@implementation AbstractController

static AbstractController *instance;

+ (AbstractController *) instance
{
    return instance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configMenus];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    self.documentsDirectory = [paths objectAtIndex:0];
    
    instance = self;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configMenus
{
    // POPOVER CATEGORY
    CategoryRepository *repo = [[CategoryRepository alloc] init];
    NSArray *data = [[NSArray alloc] initWithArray:repo.data];
    MenuCategoryController *content = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuCategoryController"];
	self.menuCategoryPopover = [[UIPopoverController alloc] initWithContentViewController:content];
	//self.menuCategoryPopover.popoverContentSize = CGSizeMake(260, data.count*50+30);
    self.menuCategoryPopover.contentViewController.preferredContentSize = CGSizeMake(260, data.count*50+30);
    self.menuCategoryPopover.popoverLayoutMargins = UIEdgeInsetsMake(0,0,0,10);
    self.menuCategoryPopover.popoverBackgroundViewClass = [CategoryPopoverBackgroundView class];
	self.menuCategoryPopover.delegate = self;
    
    //BTN HOME
    UIButton *buttonHome = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonHome setImage:[UIImage imageNamed:@"home.png"] forState:UIControlStateNormal];
    [buttonHome addTarget:self action:@selector(returnToHome:) forControlEvents:UIControlEventTouchUpInside];
    [buttonHome setFrame:CGRectMake(0, 0, 28, 20)];
    UIBarButtonItem *barButtonHome = [[UIBarButtonItem alloc] initWithCustomView:buttonHome];
    self.navigationItem.leftBarButtonItem = barButtonHome;
    
    //BTN CATEGORY
    UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonCategory setImage:[UIImage imageNamed:@"category.png"] forState:UIControlStateNormal];
    [buttonCategory addTarget:self action:@selector(openMenuCategory:) forControlEvents:UIControlEventTouchUpInside];
    [buttonCategory setFrame:CGRectMake(0, 0, 21, 20)];
    UIBarButtonItem *barButtonCategory = [[UIBarButtonItem alloc] initWithCustomView:buttonCategory];
    self.navigationItem.rightBarButtonItem = barButtonCategory;
}

-(void)configProductMenus:(int)active
{
    // POPOVER PRODUCT
    ProductRepository *productRepo = [[ProductRepository alloc] init];
    NSArray *productData = [[NSArray alloc] initWithArray: [productRepo findByCategoryId:self.product.categoryId] ];
    
    if (productData.count > 1) {
        MenuProductController *menuContent = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuProductController"];
        menuContent.product = self.product;
        self.menuProductPopover = [[UIPopoverController alloc] initWithContentViewController:menuContent];
        int w = 200;
        int h = (int) (productData.count*100+20);
        if (productData.count > 5) {
            int metade = (int) (productData.count / 2);
            if (productData.count % 2 == 1)
                metade += 1;
            //NSLog(@"%d", metade);
            w = w * 2;
            h = metade*100+20;
        }
        self.menuProductPopover.contentViewController.preferredContentSize = CGSizeMake(w, h);
        self.menuProductPopover.popoverLayoutMargins = UIEdgeInsetsMake(0,0,0,0);
        self.menuProductPopover.popoverBackgroundViewClass = [ProductPopoverBackgroundView class];
        self.menuProductPopover.delegate = self;
    }
    
    //TITLE MENU
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:18]];
    [label setText: self.product.title ];
    [label setTextColor:[UIColor whiteColor]];
    [label sizeToFit];
    CGSize fontSize = [label.text sizeWithAttributes: @{NSFontAttributeName:label.font} ];
    [label setFrame:CGRectMake((600-fontSize.width)/2, 0, fontSize.width, 20)];
    
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(label.frame.origin.x+label.frame.size.width+8, 7, 13, 6)];
    [icon setTag:(int)@"icon"];
    [icon setImage:[UIImage imageNamed:@"product-title-aba.png"]];
    CGAffineTransform transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90));
    icon.transform = transform;
    
    UIButton *btnTitle = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 600, 20)];
    [btnTitle addSubview:label];
    if (productData.count > 1) {
        [btnTitle addSubview:icon];
        [btnTitle addTarget:self action:@selector(openMenuProduct:) forControlEvents:UIControlEventTouchUpInside];
    }
    self.navigationItem.titleView = btnTitle;
    
    //FOOTER MENU
    ProductMenu *menu = [[NSBundle mainBundle] loadNibNamed:@"ProductMenu" owner:self options:nil][0];
    menu.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
    menu.frame = CGRectMake(0, 666, 1024, 40);
    menu.nav = self.navigationController;
    [menu setProduct:self.product];
    [menu setUserInteractionEnabled:YES];
    [menu setMenuActive:active];
    [self.view addSubview:menu];
}

- (NSString *) localPath:(NSString *)path
{
    return (NSString *) [self.documentsDirectory stringByAppendingPathComponent:path];
}

- (NSString *) serverUrl:(NSString *)name
{
    NSString *fname = [[NSBundle mainBundle] pathForResource:@"config" ofType:@"strings"];
    NSDictionary *d = [NSDictionary dictionaryWithContentsOfFile:fname];
    return (NSString *) [d objectForKey:name];
}

- (BOOL) checkWIfi
{
    return ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == ReachableViaWiFi);
}

- (void)returnToHome:(id)sender
{
    if (![NSStringFromClass([self.navigationController.visibleViewController class]) isEqualToString:@"CategoryController"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CategoryController *controller = [storyboard instantiateViewControllerWithIdentifier:@"CategoryController"];
        
        CATransition *transition = [CATransition animation];
        transition.type = @"reveal";
        transition.subtype = kCATransitionFromTop;
        if ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeLeft)
            transition.subtype = kCATransitionFromBottom;
        
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:controller animated:NO];
    }
}

- (void)openMenuCategory:(id)sender
{
	UIButton *tappedButton = (UIButton *)sender;
	[self.menuCategoryPopover presentPopoverFromRect:CGRectMake(tappedButton.frame.origin.x, tappedButton.frame.origin.y+26, 20, 20) inView:self.navigationController.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)openMenuProduct:(id)sender
{
	UIButton *tappedButton = (UIButton *)sender;
    
    UIImageView *icon = (UIImageView *)[self.navigationItem.titleView viewWithTag:(int)@"icon"];
    [UIView animateWithDuration:0.2 animations:^ {
        CGAffineTransform transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(0));
        icon.transform = transform;
    }];
    
    [self.menuProductPopover presentPopoverFromRect: CGRectMake(512, tappedButton.frame.origin.y+26, 20, 20)
                             inView: self.navigationController.view
                             permittedArrowDirections: UIPopoverArrowDirectionUp
                             animated:YES];
}

- (void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if ([NSStringFromClass([popoverController.contentViewController class]) isEqualToString:@"MenuProductController"]) {
        
        UIImageView *icon = (UIImageView *)[self.navigationItem.titleView viewWithTag:(int)@"icon"];
        [UIView animateWithDuration:0.2 animations:^ {
            CGAffineTransform transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90));
            icon.transform = transform;
        }];
    }
}

@end
