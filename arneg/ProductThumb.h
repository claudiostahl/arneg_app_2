//
//  ProductThumb.h
//  arneg
//
//  Created by Fabio on 11/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductThumb : UIView

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
