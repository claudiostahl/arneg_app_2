//
//  ProductMenu.m
//  arneg
//
//  Created by Fabio on 03/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductMenu.h"
#import "CategoryController.h"
#import "ProductController.h"
#import "ProductOptionController.h"
#import "ProductGalleryController.h"
#import "ProductSpecificationController.h"

@implementation ProductMenu

- (void)layoutSubviews {
    //NSLog(@"menu product");
    
    self.buttons = [[NSMutableArray alloc] init];
    
    //Personalizar
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitle:@"Personalizar" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
    [btn addTarget:self action:@selector(goPersonalizar:) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    CGSize fontSize = [btn.titleLabel.text sizeWithAttributes: @{NSFontAttributeName:btn.titleLabel.font} ];
    [btn setFrame:CGRectMake(0, 5, fontSize.width, 30)];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self addSubview:btn];
    [self.buttons addObject:btn];
    self.personalizarBtn = btn;
    
    //Opcionais
    btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitle:@"Opcionais" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
    [btn addTarget:self action:@selector(goOpcionais:) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    fontSize = [btn.titleLabel.text sizeWithAttributes: @{NSFontAttributeName:btn.titleLabel.font} ];
    [btn setFrame:CGRectMake(0, 5, fontSize.width, 30)];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self addSubview:btn];
    [self.buttons addObject:btn];
    self.opcionaisBtn = btn;
    
    //Especificações
    btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitle:@"Especificações" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
    [btn addTarget:self action:@selector(goEspecificacoes:) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    fontSize = [btn.titleLabel.text sizeWithAttributes: @{NSFontAttributeName:btn.titleLabel.font} ];
    [btn setFrame:CGRectMake(0, 5, fontSize.width, 30)];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self addSubview:btn];
    [self.buttons addObject:btn];
    self.especificacoesBtn = btn;
    
    //Fotos
    btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitle:@"Fotos" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
    [btn addTarget:self action:@selector(goFotos:) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    fontSize = [btn.titleLabel.text sizeWithAttributes: @{NSFontAttributeName:btn.titleLabel.font} ];
    [btn setFrame:CGRectMake(0, 5, fontSize.width, 30)];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self addSubview:btn];
    [self.buttons addObject:btn];
    self.fotosBtn = btn;
    
    if ([self.product.imageCustom1 isEqual: @""]) {
        self.personalizarBtn.hidden = true;
    }
    if ([self.product.imageOptions isEqual: @""]) {
        self.opcionaisBtn.hidden = true;
    }
    if ([self.product.imageSpecification isEqual: @""]) {
        self.especificacoesBtn.hidden = true;
    }
    if (self.product.photos.count == 0) {
        self.fotosBtn.hidden = true;
    }
    
    /* Sempre oculto */
    self.fotosBtn.hidden = true;
    
    [self align];
    
    btn = [self.buttons objectAtIndex:self.intActive];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)goPersonalizar:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProductController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductController"];
    [controller setProduct:self.product];
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    [self.nav.view.layer addAnimation:transition forKey:kCATransition];
    [self.nav pushViewController:controller animated:NO];
}

- (void)goOpcionais:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProductOptionController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductOptionController"];
    [controller setProduct:self.product];
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    [self.nav.view.layer addAnimation:transition forKey:kCATransition];
    [self.nav pushViewController:controller animated:NO];
}

- (void)goEspecificacoes:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProductSpecificationController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductSpecificationController"];
    [controller setProduct:self.product];
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    [self.nav.view.layer addAnimation:transition forKey:kCATransition];
    [self.nav pushViewController:controller animated:NO];
}

- (void)goFotos:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProductGalleryController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductGalleryController"];
    [controller setProduct:self.product];
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    [self.nav.view.layer addAnimation:transition forKey:kCATransition];
    [self.nav pushViewController:controller animated:NO];
}

- (void) setMenuActive:(int)n {
    self.intActive = n;
}

- (void) align {
    
    int widthTotal = 0;
    int left = 0;
    int margin = 80;
    
    NSMutableArray *visibleButtons = [[NSMutableArray alloc] init];
    for (UIButton *btn in self.buttons) {
        if (btn.hidden == false) {
            [visibleButtons addObject:btn];
            widthTotal += btn.frame.size.width + margin;
        }
    }
    
    widthTotal = widthTotal - margin;
    left = (1024 - widthTotal) / 2;
    
    for (UIButton *btn in visibleButtons) {
        CGRect frame = btn.frame;
        frame.origin.x = left;
        btn.frame = frame;
        left += frame.size.width + margin;
    }
    
}

@end
