//
//  CategoryController.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractController.h"

@interface CategoryController : AbstractController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *data;

@end
