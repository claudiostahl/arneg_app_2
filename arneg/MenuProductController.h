//
//  MenuProductController.h
//  arneg
//
//  Created by Fabio on 11/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductEntity.h"

@interface MenuProductController : UIViewController

@property ProductEntity *product;

- (void)openProduct:(UITapGestureRecognizer *)recognizer;

@end