//
//  ProductOptionController.m
//  arneg
//
//  Created by Fabio on 03/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductOptionController.h"
#import "ProductOptionEntity.h"

@implementation ProductOptionController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configProductMenus:1];
    
    self.pins = [[NSMutableArray alloc] init];
    self.pinsIsHidden = false;
    
    self.containerInfo.hidden = true;
    
    self.thumb1.clipsToBounds = YES;
    self.thumb1.layer.cornerRadius = 175;
    self.thumb1.layer.borderWidth = 6.0f;
    self.thumb1.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self.contentImage setImage:[UIImage imageNamed: self.product.imageOptionsNormal]];
    
    [self.contentScroll setMaximumZoomScale:10.0f];
    [self.contentScroll setClipsToBounds:YES];
    
    [self.view addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recognizerSingleTap:)] ];
}

- (void)viewDidAppear:(BOOL)animated {
    int i = 0;
    for (ProductOptionEntity *option in self.product.options) {
        UIButton *pin = [[UIButton alloc] initWithFrame:CGRectMake(option.x-15, option.y-15, 29, 29)];
        NSString *img = @"pin.png";
        if (option.standard)
            img = @"star.png";
        [pin setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
        pin.tag = i;
        pin.hidden = true;
        [pin addTarget:self action:@selector(openOption:) forControlEvents:UIControlEventTouchUpInside];
        [self.container addSubview:pin];
        [self.pins addObject:pin];
        i++;
    }
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(loadImageFull) userInfo:nil repeats:NO];
}

- (void) loadImageFull {
    [self.contentImage setImage:[UIImage imageNamed: self.product.imageOptions ]];
    [self showPins];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.contentImage setImage:nil];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if (self.pinsIsHidden == false) {
        [self hidePins];
    }
    return self.contentImage;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    if (!(scale > 1)) {
        [self showPins];
    }
}

- (IBAction)closeInfo:(id)sender {
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
        //[self.containerInfo setFrame:CGRectMake(0, 340, 1024, 704)];
        self.containerInfo.alpha = 0;
    } completion: ^(BOOL finished) {
        self.containerInfo.hidden = true;
        [self showPins];
    }];
}

- (void)recognizerSingleTap:(UITapGestureRecognizer *)recognizer {
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
        //[self.containerInfo setFrame:CGRectMake(0, 340, 1024, 704)];
        self.containerInfo.alpha = 0;
    } completion: ^(BOOL finished) {
        self.containerInfo.hidden = true;
        [self showPins];
    }];
}

- (void)openOption:(id)sender {
    UIButton *pin = (UIButton *)sender;
    int optionId = (int) pin.tag;
    ProductOptionEntity *option = self.product.options[optionId];
    self.titleInfo.text = option.title;
    if (option.standard)
        self.descriptionInfo.text = @"detalhe";
    else
        self.descriptionInfo.text = @"opcional";
    
    self.thumb1.hidden = false;

    if (![option.image1 isEqualToString:@""])
        [self.thumb1 setImage: [UIImage imageNamed: option.image1] ];
    else
        self.thumb1.hidden = true;
    
    [self hidePins];
    //[self.containerInfo setFrame:CGRectMake(0, 340, 1024, 704)];
    self.containerInfo.alpha = 0;
    self.containerInfo.hidden = false;
    
    [UIView animateWithDuration:0.5 animations:^ {
        //[self.containerInfo setFrame:CGRectMake(0, 0, 1024, 704)];
        self.containerInfo.alpha = 1;
    }];
}

- (void) showPins {
    for (UIImageView *pin in self.pins) {
        pin.hidden = false;
    }
    self.pinsIsHidden = FALSE;
}

- (void) hidePins {
    for (UIImageView *pin in self.pins) {
        pin.hidden = true;
    }
    self.pinsIsHidden = TRUE;
}

@end