//
//  AbstractController.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductEntity.h"
#import "CategoryEntity.h"

@interface AbstractController : UIViewController <UIPopoverControllerDelegate>

@property int categoryId;
@property ProductEntity *product;
@property NSString *documentsDirectory;
@property UIPopoverController *menuCategoryPopover;
@property UIPopoverController *menuProductPopover;

+ (AbstractController *) instance;

- (void)configProductMenus:(int)active;
- (void)configMenus;
- (NSString *) localPath:(NSString *)path;
- (NSString *) serverUrl:(NSString *)name;
- (BOOL) checkWIfi;
- (void)returnToHome:(id)sender;
- (void)openMenuCategory:(id)sender;
- (void)openMenuProduct:(id)sender;

@end
