//
//  CategoryController.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "CategoryController.h"
#import "MainController.h"
#import "ProductController.h"
#import "CategoryThumb.h"
#import "CategoryEntity.h"
#import "CategoryRepository.h"
#import "CategoryThumb.h"
#import "ProductRepository.h"

@implementation CategoryController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"arneg"];
   
    CategoryRepository *repo = [[CategoryRepository alloc] init];
    self.data = [[NSArray alloc] initWithArray:repo.data];
    
    //NSLog(@"%d", [self.data count]);
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cThumb"];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(425, 265)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setHeaderReferenceSize:CGSizeMake(0, 80)];
    [flowLayout setFooterReferenceSize:CGSizeMake(0, 80)];
    [flowLayout setMinimumLineSpacing:20.0];
    [flowLayout setSectionInset:UIEdgeInsetsMake(0, 77, 0, 77)];
    [self.collectionView setCollectionViewLayout:flowLayout];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.data count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"%d", indexPath.row);
    
    static NSString *identifier = @"cThumb";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    CategoryEntity *entity = [self.data objectAtIndex:indexPath.row];
    
    CategoryThumb *thumb = [[NSBundle mainBundle] loadNibNamed:@"CategoryThumb" owner:self options:nil][0];
    [thumb.image setImage:[UIImage imageNamed:entity.image]];
    [cell addSubview:thumb];
    
    cell.tag = entity.categoryId;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProduct:)];
    [cell addGestureRecognizer:tap];
    
    return cell;
}

- (void)openProduct:(UITapGestureRecognizer *)recognizer {
    UICollectionViewCell *cell = (UICollectionViewCell *) recognizer.view;
    int catId = (int) cell.tag;
    
    ProductRepository *repo = [[ProductRepository alloc] init];
    if ([[repo findByCategoryId:catId] count] == 0)
        return;
    ProductEntity *currentProduct = [repo findByCategoryId:catId][0];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AbstractController *controller;
    if (!([currentProduct.imageCustom1 isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductController"];
    } else if (!([currentProduct.imageOptions isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductOptionController"];
    } else if (!([currentProduct.imageSpecification isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductSpecificationController"];
    } else if (currentProduct.photos.count > 0) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductGalleryController"];
    } else {
        return;
    }
    
    controller.product = currentProduct;
    
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
        cell.alpha = 0.8;
    } completion: ^(BOOL finished) {
        [self.navigationController pushViewController:controller animated:YES];
    }];
}

@end
