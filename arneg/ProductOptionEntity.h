//
//  ProductOptionEntity.h
//  arneg
//
//  Created by Fabio on 12/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductOptionEntity : NSObject

@property int optionId;
@property NSString *title;
@property NSString *description;
@property BOOL standard;
@property NSString *image1;
@property NSString *image2;
@property NSString *image3;
@property int x;
@property int y;

@end
