import UIKit
import CocoaSecurity
import SystemConfiguration
import SVProgressHUD

public class AppUtil {
    
    func generateToken() -> String {
        return NSUUID().UUIDString
    }
    
    func showLoading() {
        SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Gradient)
    }
    
    func hideLoading() {
        SVProgressHUD.popActivity()
    }
    
    func requireConnection() {
        let alert = UIAlertController(
            title: "Sem conexão",
            message: "O aplicativo precisa de conexão com a internet. Ele vai voltar a funcionar assim que você ativar.",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{(ACTION :UIAlertAction) in
            if (!self.isConnectedToNetwork()) {
                self.currentView().presentViewController(alert, animated: true, completion: nil)
            }
        }))
        self.currentView().presentViewController(alert, animated: true, completion: nil)
    }
    
    func alertConnection() {
        let alert = UIAlertController(
            title: "Sem conexão",
            message: "O aplicativo precisa de conexão com a internet. Ele vai voltar a funcionar assim que você ativar.",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{(ACTION :UIAlertAction) in
        }))
        self.currentView().presentViewController(alert, animated: true, completion: nil)
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{(ACTION :UIAlertAction) in
        }))
        self.currentView().presentViewController(alert, animated: true, completion: nil)
    }
    
    func md5(value: String) -> String {
        return CocoaSecurity.md5(value).hexLower
    }
    
    func decryptAES(base64String:String, rawKey:String) -> String {
        let md5Key = CocoaSecurity.md5(rawKey).hexLower
        let key = md5Key.substringByInt(0, 16)
        let iv = md5Key.substringByInt(16, 32)
        let dkey = key.dataUsingEncoding(NSUTF8StringEncoding)
        let div = iv.dataUsingEncoding(NSUTF8StringEncoding)
        let resultDecrypt = CocoaSecurity.aesDecryptWithBase64(base64String, key: dkey, iv: div)
        return resultDecrypt.utf8String
    }
    
    func encryptAES(plaintext:String, rawKey:String) -> String {
        let md5Key = CocoaSecurity.md5(rawKey).hexLower
        let key = md5Key.substringByInt(0, 16)
        let iv = md5Key.substringByInt(16, 32)
        let dkey = key.dataUsingEncoding(NSUTF8StringEncoding)
        let div = iv.dataUsingEncoding(NSUTF8StringEncoding)
        let resultEncrypt = CocoaSecurity.aesEncrypt(plaintext, key: dkey, iv: div)
        return resultEncrypt.base64
    }
    
    func decodeJsonFromData(plainData: NSData) -> NSDictionary {
        let output = (try! NSJSONSerialization.JSONObjectWithData(plainData, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
        return output
    }
    
    func decodeJsonFromString(plainString: String) -> NSDictionary {
        let plainData = (plainString as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
        let output = (try! NSJSONSerialization.JSONObjectWithData(plainData, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
        return output
    }
    
    func encodeJson(data: AnyObject) -> String {
        let data2 = try? NSJSONSerialization.dataWithJSONObject(data, options: [])
        let string = NSString(data: data2!, encoding: NSUTF8StringEncoding) as! String
        print(string)
        return string
    }
    
    func decodeBase64 (base64String: String) -> String {
        let decodedData = NSData(base64EncodedString: base64String, options: NSDataBase64DecodingOptions(rawValue: 0))!
        let decodedString = NSString(data: decodedData, encoding: NSUTF8StringEncoding) as! String
        return decodedString;
    }
    
    func encodeBase64 (plainString: String) -> String {
        let plainData = (plainString as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        return base64String
    }
    
    func encodeBase64 (plainData: NSData) -> String {
        let base64String = plainData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        return base64String
    }
    
    func encryptRSA (plainString: String, publicKey: String) -> String {
        return RSA.encryptString(plainString, publicKey: decodeBase64(publicKey))
    }
    
    func currentView() -> UIViewController {
        var currentViewController: UIViewController!
        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            currentViewController = topController
        }
        return currentViewController
    }
    
    func populateString(text: AnyObject?) -> String? {
        if let text = text as? String {
            return text
        }
        return nil
    }
    
    func stringToDate(string: String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        dateFormatter.dateFormat = "yyyy-MM-dd" //'T'HH:mm:ss.SSSZ
        return dateFormatter.dateFromString(string)!
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func emptyTableView(tableView: UITableView) {
        let messageView = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        messageView.text = "Nenhum item encontrado"
        messageView.textAlignment = NSTextAlignment.Center
        messageView.sizeToFit()
        tableView.backgroundView = messageView
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
    }
}
