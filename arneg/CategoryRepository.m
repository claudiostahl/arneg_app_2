//
//  CategoryRepository.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "CategoryRepository.h"
#import "CategoryEntity.h"

@implementation CategoryRepository

static NSMutableArray *cache;

- (id) init {
    self = [super init];
    
    if (!cache) {
        NSLog(@"category load");
        self.resource = @"categories.plist";
        self.data = [[NSMutableArray alloc] init];
        
        NSArray *dataResource = [self loadDataResource];
        for (NSDictionary *item in dataResource) {
            CategoryEntity *entity = [[CategoryEntity alloc] init];
            [entity setCategoryId: (int)[[item objectForKey:@"id"] integerValue]];
            [entity setTitle:[item objectForKey:@"title"]];
            [entity setDescription:[item objectForKey:@"description"]];
            [entity setImage:[item objectForKey:@"image"]];
            [self.data addObject:entity];
            
            NSLog(@"%@", entity.title);
        }
        
        if (self.data.count > 0)
            cache = self.data;
        
        //NSLog(@"%@", self.data);
        
    } else {
        NSLog(@"category cache");
        self.data = cache;
    }
    
    return self;
}

@end
