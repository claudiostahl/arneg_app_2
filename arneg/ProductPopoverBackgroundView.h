//
//  ProductPopoverBackgroundView.h
//  arneg
//
//  Created by Fabio on 11/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductPopoverBackgroundView : UIPopoverBackgroundView {
    UIImageView *_borderImageView;
    UIImageView *_arrowView;
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;
}


@end
