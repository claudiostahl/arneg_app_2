import UIKit

public class AppUser {
    
    var hash_device: String?
    var hash_auth: String?
    var hash_refresh: String?
    var id: Int?
    var login: String?
    
    var thumb_home: String?
    var video_home: String?
    var capa_loja_landscape: String?
    var capa_loja_portrait: String?
    var capa_produto_landscape: String?
    var capa_produto_portrait: String?
    
    func save () {
        var userStorage = [String: AnyObject]()
        userStorage["hash_device"] = hash_device
        userStorage["hash_auth"] = hash_auth
        userStorage["hash_refresh"] = hash_refresh
        userStorage["id"] = id
        userStorage["login"] = login
        
        userStorage["thumb_home"] = thumb_home
        userStorage["video_home"] = video_home
        userStorage["capa_loja_landscape"] = capa_loja_landscape
        userStorage["capa_loja_portrait"] = capa_loja_portrait
        userStorage["capa_produto_landscape"] = capa_produto_landscape
        userStorage["capa_produto_portrait"] = capa_produto_portrait
        
        NSUserDefaults.standardUserDefaults().setObject(userStorage, forKey: "User")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func load () {
        if let userStorage: Dictionary<String,AnyObject> = NSUserDefaults.standardUserDefaults().objectForKey("User") as? Dictionary<String,AnyObject> {
            self.hash_device = userStorage["hash_device"] as? String
            self.hash_auth = userStorage["hash_auth"] as? String
            self.hash_refresh = userStorage["hash_refresh"] as? String
            self.id = userStorage["id"] as? Int
            self.login = userStorage["login"] as? String
            
            self.thumb_home = userStorage["thumb_home"] as? String
            self.video_home = userStorage["video_home"] as? String
            self.capa_loja_landscape = userStorage["capa_loja_landscape"] as? String
            self.capa_loja_portrait = userStorage["capa_loja_portrait"] as? String
            self.capa_produto_landscape = userStorage["capa_produto_landscape"] as? String
            self.capa_produto_portrait = userStorage["capa_produto_portrait"] as? String
        }
    }
    
    func reset () {
        self.hash_device = nil
        self.hash_auth = nil
        self.hash_refresh = nil
        self.id = nil
        self.login = nil
        
        self.thumb_home = nil
        self.video_home = nil
        self.capa_loja_landscape = nil
        self.capa_loja_portrait = nil
        self.capa_produto_landscape = nil
        self.capa_produto_portrait = nil
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey("User")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
}
