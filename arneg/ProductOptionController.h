//
//  ProductOptionController.h
//  arneg
//
//  Created by Fabio on 03/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractController.h"

@interface ProductOptionController : AbstractController<UIScrollViewDelegate>

@property NSMutableArray *pins;
@property BOOL pinsIsHidden;

@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScroll;
@property (weak, nonatomic) IBOutlet UIImageView *contentImage;

@property (weak, nonatomic) IBOutlet UIView *containerInfo;
@property (weak, nonatomic) IBOutlet UIView *contentInfo;
@property (weak, nonatomic) IBOutlet UILabel *titleInfo;
@property (weak, nonatomic) IBOutlet UILabel *descriptionInfo;
@property (weak, nonatomic) IBOutlet UIImageView *thumb1;
- (IBAction)closeInfo:(id)sender;

- (void) loadImageFull;
- (void)openOption:(id)sender;
- (void) showPins;
- (void) hidePins;

@end
