//
//  ProductSpecificationController.h
//  arneg
//
//  Created by Fabio on 03/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractController.h"

@interface ProductSpecificationController : AbstractController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *modalBg;
@property (weak, nonatomic) IBOutlet UIView *modalView;
@property (weak, nonatomic) IBOutlet UIButton *produtoBtn;
@property (weak, nonatomic) IBOutlet UIButton *caracteristicaBtn;
@property (weak, nonatomic) IBOutlet UIButton *refrigeracaoBtn;
@property (weak, nonatomic) IBOutlet UIImageView *produtoImg;
@property (weak, nonatomic) IBOutlet UIImageView *caracteristicaImg;
@property (weak, nonatomic) IBOutlet UIImageView *refrigeracaoImg;
@property UIImageView *imgView;
- (IBAction)openModalProduto:(id)sender;
- (IBAction)openModalCaracteristica:(id)sender;
- (IBAction)openModalRefrigeracao:(id)sender;

@end
