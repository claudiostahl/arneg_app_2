//
//  ProductOptionEntity.m
//  arneg
//
//  Created by Fabio on 12/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductOptionEntity.h"

@implementation ProductOptionEntity

@synthesize title, description, image1, image2, image3, optionId, standard, x, y;

@end
