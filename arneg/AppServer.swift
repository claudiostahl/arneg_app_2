import UIKit
import SwiftTryCatch

public class AppServer {

    public static let urlApi: String = AppConfig.urlBaseApi + "api/"
    public static let urlLogin: String = AppConfig.urlBaseApi + "apilogin/"
    public static let urlPing: String = AppConfig.urlBaseApi + "apiping"
    public static let urlConnect: String = AppConfig.urlBaseApi + "apiconnect"
    public static let urlEndConnect: String = AppConfig.urlBaseApi + "apiendconnect"
    public static let urlRefresh: String = AppConfig.urlBaseApi + "apirefresh"
    public static let urlLogout: String = AppConfig.urlBaseApi + "apilogout"

    var lastConnection: NSDate

    init() {
        if let lastConnection: NSDate = NSUserDefaults.standardUserDefaults().objectForKey("LastConnection") as? NSDate {
            self.lastConnection = lastConnection
        } else {
            self.lastConnection = NSDate()
        }
    }

    func connect() -> Bool {
        print("connect: " + AppServer.urlConnect, terminator: "")
        let httpData = NSData(contentsOfURL: NSURL(string: AppServer.urlConnect)!)!
        let response = AppService.util.decodeJsonFromData(httpData)
        if let key: String = response["key"] as? String {
            let rawKey = AppService.util.decodeBase64(key)
            let sign = AppService.util.generateToken()
            let dataSend = ["sign": sign]
            let dataSendJson = AppService.util.encodeJson(dataSend)
            let encrypt = RSA.encryptString(dataSendJson, publicKey: rawKey)
            let URL: NSURL = NSURL(string: AppServer.urlEndConnect)!
            let request: NSMutableURLRequest = NSMutableURLRequest(URL: URL)
            request.HTTPMethod = "POST"
            let bodyData = "data=" + encrypt + "&key=" + key
            request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
            var response: NSURLResponse?
            let responseData: NSData?
            do {
                responseData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            } catch _ {
                return false
            }
            if let response = response as? NSHTTPURLResponse, responseData = responseData {
                if (response.statusCode >= 500) {
                    return false
                } else {
                    let data = AppService.util.decodeJsonFromString(AppService.util.decryptAES(responseData.toString(), rawKey: sign));
                    if let data = data["data"] as? NSDictionary {
                        print(data)
                        if let
                        hash_auth = data["hash_auth"] as? String,
                        hash_device = data["hash_device"] as? String,
                        hash_refresh = data["hash_refresh"] as? String {
                            AppService.user.hash_auth = hash_auth
                            AppService.user.hash_device = hash_device
                            AppService.user.hash_refresh = hash_refresh
                            AppService.user.save()
                            lastConnection = NSDate()
                            NSUserDefaults.standardUserDefaults().setObject(lastConnection, forKey: "LastConnection")
                            return true
                        }
                    }
                }
            }

        }

        return false
    }

    func refresh() -> Bool {
        print("refresh", terminator: "")
        let dataSend = ["device": AppService.user.hash_device!, "refresh": AppService.user.hash_refresh!]
        let response = self.requestApi(AppServer.urlRefresh, dataSend: dataSend, key: AppService.user.hash_refresh!)
        //println(response)
        if let
        response = response,
        data = response["data"] as? NSDictionary,
        header = response["header"] as? NSDictionary,
        error = header["error"] as? Bool {
            if !error {
                print(data, terminator: "")
                if let
                hash_auth = data["hash_auth"] as? String,
                hash_device = data["hash_device"] as? String,
                hash_refresh = data["hash_refresh"] as? String {
                    print(hash_auth, terminator: "")
                    AppService.user.hash_auth = hash_auth
                    AppService.user.hash_device = hash_device
                    AppService.user.hash_refresh = hash_refresh
                    AppService.user.save()
                    lastConnection = NSDate()
                    NSUserDefaults.standardUserDefaults().setObject(lastConnection, forKey: "LastConnection")
                    return true
                }
            }
        }
        return false
    }

    func ping() -> Bool {
        if (!checkConnection()) {
            return false
        }
        let httpData = NSData(contentsOfURL: NSURL(string: AppServer.urlPing)!)
        if let _ = httpData {
            return true
        }
        return false
    }

    func login(firewall: String, login: String, password: String) -> Bool {
        if (!checkConnection()) {
            return false
        }
        let dataSend = ["login": login, "password": password]
        let response = self.requestApi(AppServer.urlLogin + "usuario", dataSend: dataSend, key: AppService.user.hash_auth!)
        if let
        response = response,
        data = response["data"] as? NSDictionary,
        header = response["header"] as? NSDictionary,
        error = header["error"] as? Bool {
            if !error {
                if let id = data["id"] as? Int {
                    AppService.user.id = id
                }
                if let login = data["email"] as? String {
                    AppService.user.login = login
                }
                AppService.user.save()
                return true
            }
        }
        return false
    }

    func logout() -> Bool {
        if (!checkConnection()) {
            return false
        }
        let dataSend = ["device": AppService.user.hash_device!, "refresh": AppService.user.hash_refresh!]
        let response = self.requestApi(AppServer.urlLogout, dataSend: dataSend, key: AppService.user.hash_refresh!, needDescrypt:false)
        if let
        response = response,
        data = response["data"] as? NSDictionary,
        header = response["header"] as? NSDictionary,
        error = header["error"] as? Bool {
            if !error {
                print(data, terminator: "")
                return true
            }
        }
        return false
    }

    func view(api: String, data: NSDictionary? = nil) -> NSDictionary? {
        if (!checkConnection()) {
            return nil
        }
        let dataSend = NSMutableDictionary()
        dataSend["device"] = AppService.user.hash_device!
        dataSend["auth"] = AppService.user.hash_auth!
        dataSend["data"] = ""
        if let data = data {
            dataSend["data"] = data
        }
        let response = self.requestApi(AppServer.urlApi + api + "/view", dataSend: dataSend, key: AppService.user.hash_auth!)
        return response
    }

    func add(api: String, data: NSDictionary? = nil) -> NSDictionary? {
        if let data = data {
            let dataSend: NSArray = [data]
            return addMany(api, data: dataSend)
        }
        return addMany(api)
    }
    
    func addMany(api: String, data: NSArray? = nil) -> NSDictionary? {
        if (!checkConnection()) {
            return nil
        }
        let dataSend = NSMutableDictionary()
        dataSend["device"] = AppService.user.hash_device!
        dataSend["auth"] = AppService.user.hash_auth!
        dataSend["data"] = ""
        if let data = data {
            dataSend["data"] = data
        }
        let response = self.requestApi(AppServer.urlApi + api + "/add", dataSend: dataSend, key: AppService.user.hash_auth!)
        return response
    }

    func edit(api: String, data: NSDictionary? = nil) -> NSDictionary? {
        if let data = data {
            let dataSend: NSArray = [data]
            return editMany(api, data: dataSend)
        }
        return editMany(api)
    }
    
    func editMany(api: String, data: NSArray? = nil) -> NSDictionary? {
        if (!checkConnection()) {
            return nil
        }
        let dataSend = NSMutableDictionary()
        dataSend["device"] = AppService.user.hash_device!
        dataSend["auth"] = AppService.user.hash_auth!
        dataSend["data"] = ""
        if let data = data {
            dataSend["data"] = data
        }
        let response = self.requestApi(AppServer.urlApi + api + "/edit", dataSend: dataSend, key: AppService.user.hash_auth!)
        return response
    }

    func remove(api: String, data: NSDictionary? = nil) -> NSDictionary? {
        if let data = data {
            let dataSend: NSArray = [data]
            return removeMany(api, data: dataSend)
        }
        return removeMany(api)
    }
    
    func removeMany(api: String, data: NSArray? = nil) -> NSDictionary? {
        if (!checkConnection()) {
            return nil
        }
        let dataSend = NSMutableDictionary()
        dataSend["device"] = AppService.user.hash_device!
        dataSend["auth"] = AppService.user.hash_auth!
        dataSend["data"] = ""
        if let data = data {
            dataSend["data"] = data
        }
        let response = self.requestApi(AppServer.urlApi + api + "/remove", dataSend: dataSend, key: AppService.user.hash_auth!)
        return response
    }

    private func requestApi(url: String, dataSend: NSDictionary, key: String, needDescrypt: Bool = true) -> NSDictionary? {
        print(url, terminator: "")
        var output: NSDictionary?
        //println(dataSend)
        let encrypt = AppService.util.encryptAES(AppService.util.encodeJson(dataSend), rawKey: key)
        //println(encrypt)
        let URL: NSURL = NSURL(string: url)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: URL)
        request.HTTPMethod = "POST"
        let bodyData = "data=" + encrypt + "&hash=" + AppService.user.hash_device!
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
        var response: NSURLResponse?
        let responseData: NSData?
        do {
            responseData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        } catch _ {
            return nil
        }
        if let response = response as? NSHTTPURLResponse, responseData = responseData {
            //println(responseData.toString())
            if (response.statusCode == 200) {
                if needDescrypt {
                    output = AppService.util.decodeJsonFromString(AppService.util.decryptAES(responseData.toString(), rawKey: key))
                } else {
                    output = AppService.util.decodeJsonFromString(responseData.toString())
                }
            } else if (response.statusCode == 400) {
                output = AppService.util.decodeJsonFromString(responseData.toString())
            } else {
                output = AppService.util.decodeJsonFromString(responseData.toString())
            }
        }
        return output
    }

    private func checkConnection() -> Bool {
        if (!AppService.util.isConnectedToNetwork()) {
            AppService.util.alertConnection()
            return false
        }
        if let _ = AppService.user.hash_device {
            if (self.lastConnection.timestamp() + 50.0 * 60.0 < NSDate().timestamp() ) {
                self.refresh()
            }
        } else {
            self.connect()
        }
        return true;
    }

}
