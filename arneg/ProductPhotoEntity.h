//
//  ProductPhotoEntity.h
//  arneg
//
//  Created by Fabio on 05/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductPhotoEntity : NSObject

@property int photoId;
@property NSString *description;
@property NSString *thumb;
@property NSString *image;
@property int position;

@end
