import UIKit
import XLForm

class LoginController: XLFormViewController {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initializeForm()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.initializeForm()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 44
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 10
        self.tableView.backgroundColor = nil
        self.tableView.alwaysBounceVertical = false
        self.tableView.scrollEnabled = false
        self.tableView.contentInset = UIEdgeInsetsMake(-35,0,0,0)
    }
    
    func initializeForm() {
        
        let form : XLFormDescriptor
        var section : XLFormSectionDescriptor
        var row : XLFormRowDescriptor
        
        form = XLFormDescriptor(title: "Login")
        
        section = XLFormSectionDescriptor.formSection()
        form.addFormSection(section)
        
        // Login
        row = XLFormRowDescriptor(tag: "Login", rowType: XLFormRowDescriptorTypeText)
        row.cellConfigAtConfigure["textField.placeholder"] = "Login"
        row.required = true
        section.addFormRow(row)
        
        row = XLFormRowDescriptor(tag: "Senha", rowType: XLFormRowDescriptorTypePassword)
        row.cellConfigAtConfigure["textField.placeholder"] = "Senha"
        row.required = true
        section.addFormRow(row)
        
        section = XLFormSectionDescriptor.formSection()
        form.addFormSection(section)
        
        row = XLFormRowDescriptor(tag: "OK", rowType: XLFormRowDescriptorTypeButton, title: "OK")
        row.cellConfig.setObject(UIColor.whiteColor(), forKey: "backgroundColor")
        row.cellConfig.setObject(UIColor(hex: 0xa51722, alpha: 1), forKey: "textLabel.textColor")
        row.action.formBlock = {[weak self](sender: XLFormRowDescriptor!) -> Void in
            self?.deselectFormRow(sender)
            let validationErrors : Array<NSError> = self!.formValidationErrors() as! Array<NSError>
            if (validationErrors.count > 0){
                AppService.util.alert("Campos inválidos", message: "Preencha todos os campos corretamente para entrar.")
                return
            }
            
            self!.tableView.endEditing(true)
            self!.enviar()
        }
        section.addFormRow(row)
        
        self.form = form
    }
    
    func enviar() {
        AppService.util.showLoading()
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            
            let values = self.formValues()
            let login = values["Login"] as! String
            let password = values["Senha"] as! String
            let response = AppService.server.login("usuario", login: login, password: password)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                AppService.util.hideLoading()
            })
            
            if (!response) {
                AppService.util.alert("E-mail ou Senha inválidos", message: "Entre em contato com a Arneg para mais informações.")
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let parent = self.parentViewController {
                    let nextViewController = parent.storyboard!.instantiateViewControllerWithIdentifier("CategoryController") as! CategoryController
                    parent.navigationController!.pushViewController(nextViewController, animated: true)
                }
            })
            
        })
    }
    
}