//
//  HomeController.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "MainController.h"
#import "CategoryController.h"
#import "CategoryRepository.h"
#import "arneg-Swift.h"
#import "Reachability.h"

@implementation MainController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home-bg.png"]];
    bgView.frame = CGRectMake(0, 0, 1024, 768);
    [self.view insertSubview:bgView atIndex:0];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self.labelLoad setText:@"verificando"];
    
    self.bridge = [[AppBridge alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    
    //remover dados
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    [fileMgr removeItemAtPath:[self localPath:@"categories.plist"] error:NULL];
    [fileMgr removeItemAtPath:[self localPath:@"products.plist"] error:NULL];
    
    [self setLabelInfo:@"atualizando categorias"];
    NSString *fileCategory = [[NSBundle mainBundle] pathForResource:@"category" ofType:@"json"];
    NSData *dataCategory = [NSData dataWithContentsOfFile:fileCategory options:NSDataReadingMappedIfSafe error:nil];
    NSDictionary *dictionaryCategory = [NSJSONSerialization JSONObjectWithData:dataCategory options:0 error:nil];
    [dictionaryCategory writeToFile:[self localPath:@"categories.plist"] atomically:YES];
    
    [self setLabelInfo:@"atualizando produtos"];
    NSString *fileProduct = [[NSBundle mainBundle] pathForResource:@"product" ofType:@"json"];
    NSData *dataProduct = [NSData dataWithContentsOfFile:fileProduct];
    NSDictionary *dictionaryProduct = [NSJSONSerialization JSONObjectWithData:dataProduct options:0 error:nil];
    [dictionaryProduct writeToFile:[self localPath:@"products.plist"] atomically:YES];
    
    [self enableClick];
    [self.labelLoad setText:@"Bem vindo"];
}

- (void) enableClick {
    [self.view addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recognizerSingleTap:)] ];
}

- (void) setLabelInfo:(NSString *)info {
    [NSThread detachNewThreadSelector:@selector(threadSetLabelInfo:) toTarget:self withObject:info];
}

- (void) threadSetLabelInfo:(NSString *)info {
    [self.labelLoad setText:info];
}

- (IBAction)openArpejo:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://arpejo.com.br"]];
}

- (void)recognizerSingleTap:(UITapGestureRecognizer *)recognizer {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoryController *controller = [storyboard instantiateViewControllerWithIdentifier:@"CategoryController"];
    [self.navigationController pushViewController:controller animated:YES];
//    if ([self.bridge isLogged]) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        CategoryController *controller = [storyboard instantiateViewControllerWithIdentifier:@"CategoryController"];
//        [self.navigationController pushViewController:controller animated:YES];
//    } else {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        CategoryController *controller = [storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
}

@end
