//
//  Product.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryEntity.h"

@interface ProductEntity : NSObject

@property int productId;
@property int categoryId;
@property CategoryEntity *category;
@property NSString *title;
@property NSString *description;
@property NSString *thumb;
@property NSString *imageSpecification;
@property NSString *imageCustom1;
@property NSString *imageCustom2;
@property NSString *imageCustom3;
@property NSString *imageOptions;
@property NSString *imageOptionsNormal;

@property NSMutableArray *options;
@property NSMutableArray *photos;
@property NSMutableArray *subproducts;

@end
