//
//  ProductRepository.h
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractRepository.h"
#import "ProductEntity.h"

@interface ProductRepository : AbstractRepository

- (NSMutableArray *) findByCategoryId:(int) categoryId;
- (ProductEntity *) loadById:(int) productId;

@end
