//
//  MenuProductController.m
//  arneg
//
//  Created by Fabio on 11/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "MenuProductController.h"
#import "AbstractController.h"
#import "ProductRepository.h"
#import "ProductEntity.h"
#import "ProductThumb.h"
#import "ProductController.h"

@interface MenuProductController ()

@end

@implementation MenuProductController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.view.superview.layer.cornerRadius = 0;
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    ProductRepository *repo = [[ProductRepository alloc] init];
    NSArray *data = [NSArray alloc];
    data = [repo findByCategoryId:self.product.categoryId];
    
    int i = 0;
    int j = 0;
    int metade = (int) (data.count / 2);
    if (data.count % 2 == 1)
        metade += 1;
    
    for (ProductEntity *entity in data) {
        ProductThumb *thumb = [[NSBundle mainBundle] loadNibNamed:@"ProductThumb" owner:self options:nil][0];
        thumb.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
        int x = 20;
        int y = i*100+10;
        if (data.count > 5 && i > metade-1) {
            y = j*100+10;
            x = 220;
            j++;
        }
        [thumb setFrame:CGRectMake(x, y, 160, 100)];
        [thumb.image setImage:[UIImage imageNamed:entity.thumb]];
        [thumb.title setText:entity.title];
        thumb.tag = entity.productId;
        
        if (i < data.count-1 && (i != metade-1 || data.count <= 5)) {
            UIView *borderBottom = [[UIView alloc] initWithFrame:CGRectMake(0, 99, 160, 1)];
            UIColor *color = [UIColor colorWithRed:115/255.0f green:115/255.0f blue:117/255.0f alpha:1.0f];
            borderBottom.backgroundColor = color;
            [thumb addSubview:borderBottom];
        }
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProduct:)];
        [thumb addGestureRecognizer:tap];
        
        [self.view addSubview:thumb];
        i++;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)openProduct:(UITapGestureRecognizer *)recognizer
{
    UIView *thumb = recognizer.view;
    
    ProductRepository *repo = [[ProductRepository alloc] init];
    ProductEntity *currentProduct = [repo loadById:(int) thumb.tag];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AbstractController *controller;
    if (!([currentProduct.imageCustom1 isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductController"];
    } else if (!([currentProduct.imageOptions isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductOptionController"];
    } else if (!([currentProduct.imageSpecification isEqual: @""])) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductSpecificationController"];
    } else if (currentProduct.photos.count > 0) {
        controller = [storyboard instantiateViewControllerWithIdentifier:@"ProductGalleryController"];
    } else {
        return;
    }
    
    controller.product = currentProduct;
    
    [AbstractController.instance.navigationController pushViewController:controller animated:YES];
    [AbstractController.instance.menuProductPopover dismissPopoverAnimated:YES];
}

@end
