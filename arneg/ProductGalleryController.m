//
//  ProductGalleryController.m
//  arneg
//
//  Created by Fabio on 04/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductGalleryController.h"
#import "ProductPhotoEntity.h"

@implementation ProductGalleryController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configProductMenus:3];
    
    self.thumbIndex = 0;
    
    self.data = self.product.photos;
    
    ProductPhotoEntity *firstPhoto = self.data[0];
    UIImage *imgZoom = [UIImage imageNamed: firstPhoto.image ];
    [self.contentImage setImage:imgZoom];
    
    [self.contentScroll setMaximumZoomScale:5.0f];
    [self.contentScroll setClipsToBounds:YES];
    
    //PREV
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(prevItemGallery:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.contentScroll addGestureRecognizer:swipeRight];
    
    //NEXT
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextItemGallery:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.contentScroll addGestureRecognizer:swipeLeft];
}

- (void)viewDidAppear:(BOOL)animated {
    //NSLog( @"viewDidAppear" );
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(116, 80)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setHeaderReferenceSize:CGSizeMake(0, 20)];
    [flowLayout setFooterReferenceSize:CGSizeMake(0, 20)];
    [flowLayout setMinimumLineSpacing:10.0];
    [flowLayout setSectionInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    
    self.thumbCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 704, 1024, 120) collectionViewLayout:flowLayout];
    [self.thumbCollection setDataSource:self];
    [self.thumbCollection setDelegate:self];
    [self.thumbCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"pPhoto"];
    [self.thumbCollection setBackgroundColor:[UIColor whiteColor]];
    
    [self.container addSubview:self.thumbCollection];
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
        CGRect f = self.thumbCollection.frame;
        f.origin.y = 546;
        self.thumbCollection.frame = f;
    } completion: ^(BOOL finished) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openThumbs:)];
        [self.contentScroll addGestureRecognizer:tap];
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.auxImage setImage:nil];
    [self.contentImage setImage:nil];
    self.data = nil;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.contentImage;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.data count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pPhoto" forIndexPath:indexPath];
    
    ProductPhotoEntity *entity = [self.data objectAtIndex:indexPath.row];
    UIImage *img = [UIImage imageNamed: entity.thumb];
    
    UIView *thumbView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 116, 80)];
    
    UIImageView *thumbImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 116, 80)];
    [thumbImageView setImage:img];
    [thumbView addSubview:thumbImageView];
    [cell addSubview:thumbView];
    
    cell.tag = indexPath.row;
    cell.layer.borderColor = [UIColor colorWithRed:228/255.0f green:58/255.0f blue:61/255.0f alpha:1.0f].CGColor;
    cell.layer.borderWidth = 0.0f;
    if (indexPath.row == 0)
        cell.layer.borderWidth = 2.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openImage:)];
    [cell addGestureRecognizer:tap];
    
    return cell;
}

- (void)openImage:(UITapGestureRecognizer *)recognizer
{
    NSLog( @"open" );
    UIView *thumb = recognizer.view;
    self.thumbIndex = (int) thumb.tag;
    [self openFullImage];
}

- (void)openThumbs:(UITapGestureRecognizer *)recognizer
{
    if (self.thumbCollection.frame.origin.y == 546) {
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
            CGRect f = self.thumbCollection.frame;
            f.origin.y = 704;
            self.thumbCollection.frame = f;
        } completion: nil];
        
    } else if (self.thumbCollection.frame.origin.y == 704) {
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
            CGRect f = self.thumbCollection.frame;
            f.origin.y = 546;
            self.thumbCollection.frame = f;
        } completion: nil];
    }
}

- (void)prevItemGallery:(UISwipeGestureRecognizer *)recognizer
{
    self.thumbIndex--;
    
    if (self.thumbIndex < 0) {
        self.thumbIndex = 0;
        return;
    }
    [self openFullImage];
}

- (void)nextItemGallery:(UISwipeGestureRecognizer *)recognizer
{
    self.thumbIndex++;
    
    if (self.thumbIndex > self.data.count-1) {
        self.thumbIndex = (int) self.data.count-1;
        return;
    }
    [self openFullImage];
}

- (void)openFullImage {
    if (self.thumbIndex == self.lastIndex)
        return;
        
    ProductPhotoEntity *entity = [self.data objectAtIndex:self.thumbIndex];
    UIImage *img = [UIImage imageNamed: entity.image];
    [self.auxImage setImage:self.contentImage.image];
    [self.contentImage setImage:img];
    
    self.auxImage.frame = CGRectMake(0, 0, 1024, 704);
    if (self.thumbIndex > self.lastIndex) {
        self.contentImage.frame = CGRectMake(1024, 0, 1024, 704);
    } else {
        self.contentImage.frame = CGRectMake(-1024, 0, 1024, 704);
    }
    
    NSInteger section = [self numberOfSectionsInCollectionView:self.thumbCollection] - 1;
    //NSInteger item = [self collectionView:self.thumbCollection numberOfItemsInSection:section] - 1;
    NSInteger item = self.thumbIndex;
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:item inSection:section];
    [self.thumbCollection scrollToItemAtIndexPath:lastIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    UICollectionViewCell *cell1 = [self.thumbCollection cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.thumbIndex inSection:0]];
    cell1.layer.borderWidth = 2.0f;
    UICollectionViewCell *cell2 = [self.thumbCollection cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.lastIndex inSection:0]];
    cell2.layer.borderWidth = 0.0f;
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
        self.contentImage.frame = CGRectMake(0, 0, 1024, 704);
        if (self.thumbIndex > self.lastIndex) {
            self.auxImage.frame = CGRectMake(-1024, 0, 1024, 704);
        } else {
            self.auxImage.frame = CGRectMake(1024, 0, 1024, 704);
        }
    } completion: nil];
    
    self.lastIndex = self.thumbIndex;
}

@end
