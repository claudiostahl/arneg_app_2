import UIKit

extension NSDate {
    
    func timestamp() -> Double {
        return self.timeIntervalSince1970 * 1000
    }
    
}