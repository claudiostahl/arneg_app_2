//
//  Product.m
//  arneg
//
//  Created by Fabio on 26/08/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import "ProductEntity.h"

@implementation ProductEntity

@synthesize productId, categoryId, category, description, title, imageCustom1, imageCustom2, imageCustom3, imageOptions, imageOptionsNormal, imageSpecification, options, photos, subproducts, thumb;

@end
