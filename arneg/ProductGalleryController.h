//
//  ProductGalleryController.h
//  arneg
//
//  Created by Fabio on 04/09/14.
//  Copyright (c) 2014 arpejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractController.h"

@interface ProductGalleryController : AbstractController<UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScroll;
@property (weak, nonatomic) IBOutlet UIImageView *contentImage;
@property (weak, nonatomic) IBOutlet UIImageView *auxImage;

@property int thumbIndex;
@property int lastIndex;
@property NSMutableArray *data;
@property UICollectionView *thumbCollection;

- (void)openImage:(UITapGestureRecognizer *)recognizer;
- (void)openThumbs:(UITapGestureRecognizer *)recognizer;
- (void)openFullImage ;

@end