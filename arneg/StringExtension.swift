import Foundation

extension String {
    
    static func className(aClass: AnyClass) -> String
    {
        return NSStringFromClass(aClass).componentsSeparatedByString(".").last!
    }
    
    func replace(target: String, withString: String) -> String
    {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func indexByInt(index: Int) -> Index {
        if index >= 0 {
            return self.startIndex.advancedBy(index)
        } else {
            return self.endIndex.advancedBy(index)
        }
    }
    
    func substringByInt(start: Int, _ end: Int? = nil) -> String {
        let startIndex = self.indexByInt(start)
        let endIndex = end != nil ? self.indexByInt(end!) : self.endIndex
        if startIndex <= endIndex {
            return self.substringWithRange(startIndex..<endIndex)
        } else {
            return ""
        }
    }
    
    func substringByInt(range: Range<Int>) -> String {
        return self.substringByInt(range.startIndex, range.endIndex)
    }
    
}