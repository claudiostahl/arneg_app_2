//
//  Exemplo.swift
//  arneg
//
//  Created by Claudio Stahl on 12/11/15.
//  Copyright © 2015 arpejo. All rights reserved.
//

import Foundation

@objc class AppBridge : NSObject {
    
    override init() {
        AppService.user.load()
    }
    
    func isLogged() -> Bool {
        if let _ = AppService.user.id {
            return true
        } else {
            if (!AppService.util.isConnectedToNetwork()) {
                AppService.util.requireConnection()
            }
            return false
        }
    }
    
}